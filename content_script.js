var script = document.createElement('script');
script.id = 'tmpScript';
script.appendChild(document.createTextNode('document.body.dataset.ddl = JSON.stringify(window.digitalData)'));
document.body.appendChild(script);

var payload = {
  'window.digitalData': JSON.parse(document.body.dataset.ddl)
}

var lytics = document.querySelectorAll('[data-analytics]');
for (var i = 0; i < lytics.length; i++) {
  var tag = lytics[i].tagName.toLowerCase();
  if (lytics[i].className !== '') {
     tag = tag + '.' + lytics[i].className.replace(/\s/g, '.')
  }
  if (payload[tag] === undefined) {
    payload[tag] = [];
  }
  try {
    var ret = JSON.parse(lytics[i].dataset.analytics)
  } catch(err) {
    continue;
  }
  payload[tag].push(ret);
}

var keys = Object.keys(payload)
for (var i = keys.length; i--;) {
  if (payload[keys[i]].length === 0) {
    delete payload[keys[i]];
  } else if (payload[keys[i]].length === 1) {
    payload[keys[i]] = payload[keys[i]][0];
  }
}

chrome.runtime.sendMessage(payload);
