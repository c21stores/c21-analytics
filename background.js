// When the extension is installed or upgraded ...
chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That fires when a page's URL contains our domain ...
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlContains: 'c21' },
          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});

chrome.pageAction.onClicked.addListener(function(tab) {
  var vm = tab;
  chrome.windows.create(
    {
      url: 'popup.html',
      type: 'popup',
      left: 600,
      top: 300,
      width: 720,
      height: 480
    }, 
    (function() {
      var vm = this;
      chrome.tabs.executeScript(
        vm.id,
        {
          file: 'optimal-select.min.js'
        },
        (function() {
          var vm = this;
          chrome.tabs.executeScript(
            vm.id,
            {
              file: 'content_script.js'
            });
        }).bind(vm));
    }).bind(vm));
});
